
/* ----------Bài 1 ---------- */
function tinhTienThue(){
    var hoTen = document.getElementById("txt-ho-ten").value;
    var tongThuNhap = document.getElementById("txt-thu-nhap").value*1;
    var soNguoiPhuThuoc = document.getElementById("txt-nguoi-phu-thuoc").value*1;
    var thuNhapChiuThue = tongThuNhap - 4e+6 - soNguoiPhuThuoc * 16e+5;

    var tongThue = new Intl.NumberFormat('vn-VN', {style: 'currency', currency: "VND"}).format(tinhThue(thuNhapChiuThue));

    var result1El = document.getElementById("txt-hien-thi").setAttribute('value',`Tiền thuế thu nhập cá nhân của ${hoTen}: ${tongThue}`);
}


function tinhThue(chiuThue){
    var tienThue = 0;
    if(chiuThue<=60e+6)
        tienThue = chiuThue*0.05;
    else if(chiuThue>60e+6 && chiuThue <=120e+6)
        tienThue = (60e+6)*0.05+(chiuThue - 60e+6)*0.1;
    else if(chiuThue>120e+6 && chiuThue <=210e+6)
        tienThue = (60e+6)*0.05+(120e+6)*0.1+(chiuThue - 180e+6)*0.15;
    else if(chiuThue>210e+6 && chiuThue <=384e+6)
        tienThue = (60e+6)*0.05+(120e+6)*0.1 +(210e+6)*0.15+(chiuThue - 390e+6)*0.2;
    else if(chiuThue>384e+6 && chiuThue <=624e+6)
        tienThue = (60e+6)*0.05+(120e+6)*0.1 +(210e+6)*0.15+(384e+6)*0.2+(chiuThue - 774e+6)*0.25;
    else if(chiuThue>624e+6 && chiuThue <=960e+6)
        tienThue = (60e+6)*0.05+(120e+6)*0.1 +(210e+6)*0.15+(384e+6)*0.2+(624e+6)*0.25+(chiuThue - 1398e+6)*0.3;
    else
        tienThue = (60e+6)*0.05+(120e+6)*0.1 +(210e+6)*0.15+(384e+6)*0.2+(624e+6)*0.25+(960e+6)*0.3+(chiuThue - 2358e+6)*0.35;
    return tienThue;
}

/* ----------Bài 2 ---------- */
document.getElementById("txt-so-ket-noi").style.display = "none";

function showDiv(divId, element)
{
    document.getElementById(divId).style.display = element.value == 1 ? 'block' : 'none';
}

function tinhTienCap(){
    var maSoKhachHang = document.getElementById("txt-ma-kh").value;
    var soKenhCaoCap = document.getElementById("txt-so-kenh").value*1;
    var loaiKhachHang = document.getElementById("select-customer").value;
    var soKetNoi = document.getElementById("txt-so-ket-noi").value*1;
    if(loaiKhachHang == "0"){
        var result2El = document.getElementById("txt-tinh").setAttribute('value',`Mã khách hàng:${maSoKhachHang} có tiền cáp $${nhaDan(soKenhCaoCap)}`);
    }
    else if(loaiKhachHang == "1"){
        var result2El = document.getElementById("txt-tinh").setAttribute('value',`Mã khách hàng doanh nghiệp: ${maSoKhachHang} có tiền cáp $${doanhNghiep(soKenhCaoCap, soKetNoi)}`);
    }
    else
        alert("Vui lòng hãy chọn loại khách hàng");
}

function nhaDan(soKenh){
    const phiHoaDon = 4.5;
    const phiDichVu = 20.5;
    const phiThue = 7.5;
    var tongPhi = phiHoaDon+phiDichVu+phiThue*soKenh;
    return tongPhi;
}

function doanhNghiep(soKenh, soKetNoi){
    const phiHoaDon = 15;
    var phiDichVu;
    if(soKetNoi <=10)
        phiDichVu = 75;
    else
        phiDichVu = 75+(soKetNoi-10)*5;
    const phiThue = 50;
    var tongPhi = phiHoaDon+phiDichVu+phiThue*soKenh;
    return tongPhi;
}



function bai1(){
    document.getElementById("e1").style.display = 'block';
    document.getElementById("e2").style.display = "none";
}

function bai2(){
    document.getElementById("e2").style.display = 'block';
    document.getElementById("e1").style.display = "none";
}
